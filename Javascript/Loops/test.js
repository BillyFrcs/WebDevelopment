function basicLoops(num)
{
     while (num < 10) {
          num++;
          console.log(num);
     }
}

basicLoops(0);

let input = prompt('Enter number: ');
for (let i = 1; i <= input; i++) {
     console.log(i);
}