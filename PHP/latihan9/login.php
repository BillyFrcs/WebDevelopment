<?php
require 'function.php';

if (isset($_POST['submit'])) {
     $username = $_POST['username'];
     $pwd = $_POST['pwd'];

     $result = mysqli_query($conn, "SELECT * FROM users WHERE username = '$username'");

     if (mysqli_num_rows($result) === 1) {
          $row = mysqli_fetch_assoc($result);

          if (password_verify($pwd, $row['password'])) {
               header("Location: index.php");

               exit;
          }
     }

     $error = true;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="loginstyle.css">
     <title>Login</title>
</head>

<body>
     <h1>Login</h1>
     <hr>
     <form action="" method="post">
          <div class="container">
               <label for="username"><b>Username</b></label>
               <input type="text" placeholder="Masukan Username" name="username" required>
               <label for="pwd"><b>Password</b></label>
               <input type="password" placeholder="Masukan Password" name="pwd" required>

               <?php if (isset($error)) : ?>
                    <p style="color:red; font-style: italic;">username / password salah!</p>
               <?php endif; ?>

               <button type="submit" name="submit">Login</button>

               <label>
                    <input type="checkbox" checked="checked" name="remember"> Remember me
               </label>
          </div>
          <div class="container" style="background-color:#f1f1f1">
               <button type="button" class="cancelbtn">Cancel</button>
               <span class="psw">Forgot <a href="#">password?</a></span>
          </div>
     </form>
</body>

</html>