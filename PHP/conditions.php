<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Document</title>
</head>

<body>
     <?php
     $value = 19;

     if ($value <= 19) {
          echo "Less than 19";
     } else {
          echo "Greater than or equal to 19";
     }

     // write switch statement
     $game = 'PUBG';
     switch ($game) {
          case 'PUBG':
               echo "Play PUBG";
               break;
          case 'Fortnite':
               echo "Play Fortnite";
               break;
          case 'Overwatch':
               echo "Play Overwatch";
               break;
          default:
               echo "Play other games";
               break;
     }
     ?>

</body>

</html>