let id = $("input[name*='user_id']")
id.attr("readonly", "readonly");

$(".btnedit").click(e =>
{
    let textValues = displayData(e);

    let name = $("input[name*='user_name']");
    let birthPlace = $("input[name*='birth_place']");
    let age = $("input[name*='age']");

    id.val(textValues[0]);
    name.val(textValues[1]);
    birthPlace.val(textValues[2]);
    age.val(textValues[3].replace("$", ""));
});

function displayData(e)
{
    let id = 0;
    const td = $("#tbody tr td");
    let textValues = [];

    for (const value of td) {
        if (value.dataset.id == e.target.dataset.id) {
            textValues[id++] = value.textContent;
        }
    }

    return textValues;
}