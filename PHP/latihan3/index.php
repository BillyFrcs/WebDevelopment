<?php
require 'functions.php';

$Students = query("SELECT * FROM mahasiswa");
?>

<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>List Data Student</title>
</head>
<body>
     <h1>List Data Student</h1>

     <table border="1" cellpadding="5" cellspacing="0">
          <tr>
               <th>No</th>
               <th>Nama</th>
               <th>NIM</th>
               <th>Jurusan</th>
               <th>Gambar</th>
               <th>Action</th>
          </tr>

          <?php $i = 1; ?>
          <?php foreach ($Students as $student) : ?>
           <tr>
                <td><?= $i;?> </td>
                <td><?= $student['nama']; ?></td>
                <td><?= $student['nim']; ?></td>
                <td><?= $student['jurusan']; ?></td>
                <td>
                     <img src="<?= $student['gambar']; ?>" alt="image">
                </td>
                <td>
                     <a href ="">Edit</a>
                     <a href ="">Delete</a>
                </td>
          </tr> 
          <?php $i++; ?>   
          <?php endforeach; ?>
     </table>
</body>
</html>