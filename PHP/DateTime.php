<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Date & Time</title>
</head>
<body>
     <h2>Date</h2>
     <?php
     echo date('l, d F Y');
     echo "<br> Today is " . date("l");

     echo "<br>";

     $date = mktime(16, 50, 30, 3, 8, 2022);
     echo "Created date is " . date("Y-m-d h:i:sa", $date);
     ?>

     <h2>Time</h2>
     <?php
     # Get central european time
     $today = date("D M j G:i:s T Y");
     echo $today . "<br>";

     # Current Time
     date_default_timezone_set("America/Los_Angeles");
     echo "The time is " . date("h:i:sa");
     ?>
</body>
</html>