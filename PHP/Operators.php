<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Operator</title>
</head>
<body>
     <h2>Arithmetic Operator</h2>
     <?php
     // Arithmetic Operators
     $x = 6;
     $y = 8;
     $z = 10;

     echo "Addition: " . $x + $y + $z . "<br>";
     echo "Subtract: " . $x - $y - $z . "<br>";
     echo "Multiply: " . $x * $y * $z . "<br>";
     echo "Divide: " . $x / $y / $z . "<br>";
     echo "Modulo: " . $x % $y % $z . "<br>";
     ?>

     <h2>Assignment Operator</h2>
     <?php
          // Assignment Operator
          $a = 5;
          $a += 10; // Increment
          echo "Increment: " . $a . "<br>";

          $b = 20; 
          $b -= 10; // Decrement
          echo "Decrement: " . $b . "<br>";

          $c = 2;
          $c *= 6; // Multiply
          echo "Multiply: " . $c . "<br>";

          $d = 6;
          $d /= 3; // Divide
          echo "Divide: " . $d . "<br>";

          $e = 5;
          $e %= 2; // Modulo
          echo "Modulo: " . $e . "<br>";
     ?>

     <h2>Concatenation Operator</h2>
     <?php
     // Concatenation operator
     $first_name = "Billy";
     $last_name = "Franscois";

     echo "Full Name: " . $first_name . " " . $last_name . "<br>";
     ?>

     <h2>Comparison Operator</h2>
     <?php
     // Comparison Operator
     $a = 5;
     $b = 9;
     var_dump($a >= $b ? "True" : "False");

     $c = 10;
     $d = 9;
     var_dump($c < $d ? "True" : "False");

     $e = 80;
     $f = 80;
     var_dump($e == $f);

     $g = 7;
     $h = 68;
     var_dump($g !== $h);
     ?>

     <h2>Logical Operator</h2>
     <?php
     // Logical Operator
     $x = 89;
     $y = 95;

     // AND &&
     if ($x < 100 && $y > 90) {
          echo "Both condition are True";
     } else {
          echo "Both condition are False";
     }

     // OR ||
     if ($x == 100 || $y >= 55) {
          echo "<br>One of the condition is True";
     } else {
          echo "<br>One of the condition is False";
     }

     $is_run = false;
     if (!$is_run){
          echo "<br>The condition is False";
     } else {
          echo "<br>The condition is True";
     }
     ?>
</body>
</html>