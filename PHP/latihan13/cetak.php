<a href="insert.php">Tambah Data</a> | <a href="cetak.php">Print</a><br>

<?php

session_start();
if (isset($_SESSION["login"])) {
     header("Location: login.php");
     exit;
}

require 'function.php';

$mahasiswa = query("SELECT * FROM mahasiswa");

if (isset($_POST["cari"])) {
     $mahasiswa = cari($_POST["keyword"]);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Daftar Mahasiswa</title>
</head>

<body>
     <h1>Daftar Mahasiswa</h1>
     <hr><br>
     <table border="1" cellpadding="10" cellspacing="0">
          <tr>
               <th>No.</th>
               <th>Nama</th>
               <th>NIM</th>
               <th>Jurusan</th>
               <th>Gambar</th>
          </tr>
          <?php $i = 1; ?>
          <?php foreach ($mahasiswa as $mhs) : ?>
               <tr>
                    <td><?= $i; ?></td>
                    <td><?= $mhs["nama"]; ?></td>
                    <td><?= $mhs["nim"]; ?></td>
                    <td><?= $mhs["jurusan"]; ?></td>
                    <td>
                         <img src="<?= $mhs["gambar"]; ?>" alt="">
                    </td>
               </tr>
               <?php $i++; ?>
          <?php endforeach; ?>
     </table>
</body>

</html>
<script>
     window.print();
</script>