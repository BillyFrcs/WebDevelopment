<?php 
     // array associative
     $data_student = Array("Billy" => Array("name" => "Billy Franscois", 
     "NIM" => "12155201200012",
     "address" => "New York", 
     "dateOfBirth" => "19-1-2000", 
     "generation" => "2020", 
     "majors" => "Informatika"), 

     "Eren" => Array("name" => "Eren Yeager", 
     "NIM" => "12155201200013",
     "address" => "Tokyo", 
     "dateOfBirth" => "20-9-1999", 
     "generation" => "2021", 
     "majors" => "Ekonomi"),

     "Levi" => Array("name" => "Levi Ackerman", 
     "NIM" => "12155201200010",
     "address" => "New York", 
     "dateOfBirth" => "12-12-2003", 
     "generation" => "2019", 
     "majors" => "Komputer")
     )
?>

<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Data Mahasiswa</title>
</head>
<body>
     <h1>Data Mahasiswa</h1>

     <hr>

     <ul>
          <?php 
          foreach($data_student as $key => $data) : ?>
          <li><?= $data["name"]; ?></li>
          <li><?= $data["NIM"]; ?></li>
          <li><?= $data["address"]; ?></li>
          <li><?= $data["dateOfBirth"]; ?></li>
          <li><?= $data["generation"]; ?></li>
          <li><?= $data["majors"]; ?></li>
          <br>
          <?php endforeach;
          ?>
     </ul>
</body>
</html>