// Responsive navbar toggle navigation
const toggleBtn = document.querySelector('.navbar-toggle-btn');
const toggleBtnIcon = document.querySelector('.navbar-toggle-btn .hamburger');
const dropDownMenu = document.querySelector('.dropdown-menu');

toggleBtn.onclick = () => {
    dropDownMenu.classList.toggle('open');

    const isOpen = dropDownMenu.classList.contains('open');

    toggleBtnIcon.classList = isOpen ? 'fa-solid fa-xmark' : 'fa-solid fa-bars';
}

// Image slider
let sliders = document.querySelectorAll('.slider');
let prevBtn = document.querySelector('#prev');
let nextBtn = document.querySelector('#next');
let indicators = document.querySelector('#indicators');
let current = 0;

nextBtn.addEventListener('click', nextImages);
prevBtn.addEventListener('click', previousImages);

function createDots() {
    sliders.forEach((slider) => {

        let li = document.createElement("li");

        indicators.appendChild(li);
    });
}

createDots();

let indicatorsLi = document.querySelectorAll('#indicators li');

function addActiveDots(n) {

    indicatorsLi.forEach((li) => {
        li.classList.remove('active');
    });

    indicatorsLi[n].classList.add('active');
}

addActiveDots(current);

function showImages(n) {
    sliders.forEach((slide) => {
        slide.classList.remove('active');
    });

    sliders[n].classList.add('active');
}

function nextImages() {
    if (current < sliders.length - 1) {
        current++;
    } else {
        current = 0;
    }

    showImages(current);
    addActiveDots(current);
}

function previousImages() {
    if (current <= 0) {
        current = sliders.length - 1;
    } else {
        current--;
    }

    showImages(current);
    addActiveDots(current);
}

function dotsButton() {
    indicatorsLi.forEach((li, i) => {
        li.addEventListener("click", () => {
            current = i;
            showImages(current);
            addActiveDots(current);
        });
    });
}

dotsButton();

setInterval(() => {
    nextImages();
    addActiveDots(current);
}, 5000);

window.scrollTo({ top: 900, behavior: 'smooth' })